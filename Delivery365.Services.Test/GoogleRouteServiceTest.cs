﻿#region

using System.Collections.Generic;
using System.Linq;
using Delivery365.Events.Delivery;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;

#endregion

namespace Delivery365.Services.Test
{
    [TestFixture]
    public class GoogleRouteServiceTest
    {
        [Test]
        public void TestGoogleRouteService()
        {
            var service = new GoogleRouteService();
            var deliveryRouted = new DeliveryRouted
            {
                OriginAddress = "200 Queen Street, Melbourne, VIC 3000",
                EndAddress = "200 Queen Street, Melbourne, VIC 3000",
                RouteLegs = new List<RouteLeg>
                {
                    new RouteLeg {Address = "14 Horfield Avenue, Box Hill North, VIC 3129"},
                    new RouteLeg {Address = "222/9 The Arcade, Docklands, VIC 3008"},
                    new RouteLeg {Address = "5 Tarella Dr, Mt Waverley, VIC 3149"},
                }
            };

            service.Handle(deliveryRouted);

            Assert.AreEqual(50285, deliveryRouted.TotalDistance);
            Assert.AreEqual(3815, deliveryRouted.TotalDuration);
        }
    }

}