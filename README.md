# Delivery@365#


### What is this repository for? ###

* **Delivery365** is a order delivery solution that helping small business user from manage delivery orders, delivery trip planning and tracking. 

### Project Highlight ###
* AngularJS, HTML5
* Asp.net WebAPI, OWIN-compatible 
* DDD, CQRS, Event sourcing architecture pattern  
* Siaqodb NoSQL embedded object and document database engine
* Google map API


### How do I get set up? ###

1. Just clone the repository
2. Make sure you have lastest version of nuget installed
3. Build the project and nuget packages will be auto restored
4. Create a empty database with name "applicationDB" on your local sql express or pointing to other database by updating connection string in web.config
5. F5

### Get Started ###
1. After sign in, GOTO [Create Order], key in Customer name and address, click [Save]
2. Repeat creating few orders with different valid addresses.
3. GOTO [Orders], select checked all orders, click [Create Delivery Trip], you will be redirected to [Delivery Details] page after this.
4. List of order destinations listed in [Delivery Details] page.

#### All routed destination featured with following information : ####
1. Optimized travel sequence
2. Distance and duration between each destination
3. Estimated time of arriving (ETA)

### What next? ###
* Bug fixing, UX enhancement.
* More features about order management and trip planning
* Machine learning to improve accuracy of ETA calculation
* User role and mobile client for delivery agent update tracking status on move
* API to allow third party integration
* Multi tenancy, user on board experience
* more ... 

### Who do I talk to? ###

* if you have any issue/ idea, please feel free to share it with me : emmanuelkong@gmail.com