## The beauty of CQRS & Event Sourcing ##

After a long battle with this architure pattern, I'm finally got it implemented in my application. 
In nutsheel, DDD, CQRS or Event Sourcing provide a clean way of helping you focus on getting domains and bussiness logics right at the first place before other things like UI, data persistency or infrastructure.

This helped alot in developing a system with complex domains and business logic.

There are a lot of implemetations of CQRS & event sourcing, Edument.CQRS (http://cqrs.nu/) has been choosen due to it's simplexity and fidelity which perfect for a first learner.