﻿#region

using System;
using Delivery365.Domain.Order;
using Delivery365.Events.Delivery;
using Delivery365.Events.Order;
using Delivery365.Events.Shared;
using NUnit.Framework;

#endregion

namespace Delivery365.Tests
{
    [TestFixture]
    public class OrderTests : BDDTest<OrderAggregate>
    {
        [SetUp]
        public void Setup()
        {
            //TestContext.
            testId = Guid.NewGuid();
            testDateTime = new DateTime(2014, 1, 1, 0, 0, 0);
        }

        private Guid testId;
        private DateTime testDateTime;

        [Test]
        public void CanCompleteOrder()
        {
            Test(
                Given(),
                When(new CompleteOrder
                {
                    Id = testId,
                    DateTime = testDateTime,
                    ReceivedBy = "A"
                }),
                Then(new OrderCompleted
                {
                    Id = testId,
                    Completed = testDateTime,
                    ReceivedBy = "A",
                    Status = "Completed",
                    DeliveryCompleted = false
                }));
        }

        [Test]
        public void CanCreateOrder()
        {
            Test(
                Given(),
                When(new CreateOrder
                {
                    Id = testId,
                    DateTime = testDateTime,
                    Customer = new Customer
                    {
                        Name = "Name",
                        Phone = "04041111111",
                        Email = "abd@abc.com"
                    },
                    ShippingAddress = new Address
                    {
                        Street1 = "1 Spencer Street",
                        Suburb = "Melbourne",
                        PostCode = 3000,
                        State = "VIC"
                    }
                }),
                Then(new OrderCreated
                {
                    Id = testId,
                    Created = testDateTime,
                    Customer = new Customer
                    {
                        Name = "Name",
                        Phone = "04041111111",
                        Email = "abd@abc.com"
                    },
                    ShippingAddress = new Address
                    {
                        Street1 = "1 Spencer Street",
                        Suburb = "Melbourne",
                        PostCode = 3000,
                        State = "VIC"
                    },
                    Status = "New"
                }));
        }
    }
}