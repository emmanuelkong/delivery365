﻿#region

using System;
using System.Collections.Generic;
using Delivery365.Domain.Delivery;
using Delivery365.Events.Delivery;
using Delivery365.Events.Order;
using NUnit.Framework;

#endregion

namespace Delivery365.Tests
{
    [TestFixture]
    public class DeliveryTests : BDDTest<DeliveryAggregate>
    {
        [SetUp]
        public void Setup()
        {
            //TestContext.
            testId = Guid.NewGuid();
            testDateTime = new DateTime(2014, 1, 1, 0, 0, 0);
        }

        private Guid testId;
        private DateTime testDateTime;

        [Test]
        public void CanCreateDelivery()
        {
            Test(
                Given(),
                When(new CreateDelivery
                {
                    Id = testId,
                    DateTime = testDateTime,
                    OriginAddress = "Origin",
                    EndAddress = "End",
                    DeliveryItems =
                        new List<DeliveryItem>
                        {
                            new DeliveryItem
                            {
                                OrderId = new Guid("1cfdb857-7d28-43bb-8644-ed2d5570f87d"),
                                Address = "Address A"
                            },
                            new DeliveryItem
                            {
                                OrderId = new Guid("1f5b002e-e4b8-411a-9457-433a1d93f5fb"),
                                Address = "Address B"
                            }
                        }
                }),
                Then(
                    new DeliveryCreated
                    {
                        Id = testId,
                        Created = testDateTime,
                        OriginAddress = "Origin",
                        EndAddress = "End",
                        Status = "New",
                        DeliveryItems =
                            new List<DeliveryItem>
                            {
                                new DeliveryItem
                                {
                                    OrderId = new Guid("1cfdb857-7d28-43bb-8644-ed2d5570f87d"),
                                    Address = "Address A"
                                },
                                new DeliveryItem
                                {
                                    OrderId = new Guid("1f5b002e-e4b8-411a-9457-433a1d93f5fb"),
                                    Address = "Address B"
                                }
                            }
                    },
                    new OrderDeliveryPlanned
                    {
                        Id = new Guid("1cfdb857-7d28-43bb-8644-ed2d5570f87d"),
                        DeliveryId = testId,
                        DateTime = testDateTime,
                        Status = "Planned"
                    },
                    new OrderDeliveryPlanned
                    {
                        Id = new Guid("1f5b002e-e4b8-411a-9457-433a1d93f5fb"),
                        DeliveryId = testId,
                        DateTime = testDateTime,
                        Status = "Planned"
                    }
                    ));
        }

        [Test]
        public void CanMarkDeliveryStarted()
        {
            Test(
                Given(new DeliveryCreated
                {
                    Id = testId,
                    Created = testDateTime,
                    OriginAddress = "Origin",
                    EndAddress = "End",
                    Status = "New",
                    DeliveryItems =
                        new List<DeliveryItem>
                        {
                            new DeliveryItem
                            {
                                OrderId = new Guid("1cfdb857-7d28-43bb-8644-ed2d5570f87d"),
                                Address = "Address A"
                            },
                            new DeliveryItem
                            {
                                OrderId = new Guid("1f5b002e-e4b8-411a-9457-433a1d93f5fb"),
                                Address = "Address B"
                            }
                        }
                }, new DeliveryRouted
                {
                    Id = testId,
                    Routed = testDateTime,
                    RouteLegs = new List<RouteLeg>
                    {
                        new RouteLeg
                        {
                            Id = new Guid("1cfdb857-7d28-43bb-8644-ed2d5570f87d"),
                            Address = "Address A"
                        },
                        new RouteLeg
                        {
                            Id = new Guid("1f5b002e-e4b8-411a-9457-433a1d93f5fb"),
                            Address = "Address B"
                        }
                    }
                }),
                When(new StartDelivery
                {
                    Id = testId,
                    DateTime = testDateTime
                }),
                Then(new DeliveryStarted
                {
                    Id = testId,
                    Started = testDateTime,
                    Status = "Started"
                }));
        }

        [Test]
        public void CanRouteDelivery()
        {
            Test(
                Given(new DeliveryCreated
                {
                    Id = testId,
                    Created = testDateTime,
                    OriginAddress = "Origin",
                    EndAddress = "End",
                    DeliveryItems =
                        new List<DeliveryItem>
                        {
                            new DeliveryItem
                            {
                                OrderId = new Guid("1cfdb857-7d28-43bb-8644-ed2d5570f87d"),
                                Address = "Address A"
                            },
                            new DeliveryItem
                            {
                                OrderId = new Guid("1f5b002e-e4b8-411a-9457-433a1d93f5fb"),
                                Address = "Address B"
                            }
                        }
                }),
                When(new RouteDelivery
                {
                    Id = testId,
                    DateTime = testDateTime
                }),
                Then(
                    new DeliveryRouted
                    {
                        Id = testId,
                        Routed = testDateTime,
                        OriginAddress = "Origin",
                        EndAddress = "End",
                        Status = "Routed",
                        RouteLegs = new List<RouteLeg>
                        {
                            new RouteLeg
                            {
                                Id = new Guid("1cfdb857-7d28-43bb-8644-ed2d5570f87d"),
                                Address = "Address A"
                            },
                            new RouteLeg
                            {
                                Id = new Guid("1f5b002e-e4b8-411a-9457-433a1d93f5fb"),
                                Address = "Address B"
                            }
                        }
                    }
                    ));
        }

        [Test]
        public void DeliveryCanNotBeMarkedStartedTwice()
        {
            Test(
                Given(new StartDelivery
                {
                    Id = testId,
                    DateTime = testDateTime
                }),
                When(new StartDelivery
                {
                    Id = testId,
                    DateTime = testDateTime
                }),
                ThenFailWith<DeliveryAlreadyStarted>());
        }
    }
}