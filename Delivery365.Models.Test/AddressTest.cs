﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delivery365.Events.Shared;
using NUnit.Framework;

namespace Delivery365.Models.Test
{
    [TestFixture]
    public class AddressTest
    {
        [Test]
        public void TestAddressModel()
        {
            //Test address with street line 1 only
            var address1 = new Address
            {
                Street1 = "1 Spencer Street",
                Suburb = "Melbourne",
                PostCode = 3000,
                State = "VIC"
            };

            Assert.AreEqual("1 Spencer Street, Melbourne, 3000 VIC", address1.ToString());

            //Test address with street line 2
            var address2 = new Address
            {
                Street1 = "1 Building",
                Street2 = "Spencer Street",
                Suburb = "Melbourne",
                PostCode = 3000,
                State = "VIC"
            };

            Assert.AreEqual("1 Building, Spencer Street, Melbourne, 3000 VIC", address2.ToString());

            //Test invalid address
            var address3 = new Address
            {
                Street2 = "Spencer Street",
                Suburb = "Melbourne",
                PostCode = 3000,
                State = "VIC"
            };

            Assert.IsFalse(address3.IsValid);
            Assert.AreEqual("Invalid Address", address3.ToString());
        }
    }
}
