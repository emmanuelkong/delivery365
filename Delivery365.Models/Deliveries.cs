﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Delivery365.Events.Delivery;
using Delivery365.Events.Order;
using Edument.CQRS;
using Sqo;

#endregion

namespace Delivery365.Models
{
    public class Deliveries : IDeliveryQueries, 
        ISubscribeTo<DeliveryCreated>,
        ISubscribeTo<DeliveryRouted>,
        ISubscribeTo<DeliveryStarted>,
        ISubscribeTo<OrderCompleted>

    {
        private readonly ISiaqodb _siaqodb;

        public Deliveries(ISiaqodb siaqodb)
        {
            _siaqodb = siaqodb;
        }

        public Delivery Get(Guid id)
        {
            lock (_siaqodb)
                return (from Delivery delivery in _siaqodb
                    where delivery.Id == id
                    select delivery).SqoFirstOrDefault();
        }

        public List<Delivery> GetAll()
        {
            lock (_siaqodb)
                return (from Delivery delivery in _siaqodb
                        select delivery).ToList();
        }

        public void Handle(DeliveryCreated e)
        {
            Mapper.CreateMap<DeliveryCreated, Delivery>();

            var deliveryBeingCreated = Mapper.Map<Delivery>(e);

            lock (_siaqodb)
            {
                deliveryBeingCreated.Orders = new List<Order>();
                foreach (var deliveryItem in e.DeliveryItems)
                {
                    var order = (from Order o in _siaqodb
                                 where o.Id == deliveryItem.OrderId
                                 select o).SqoFirstOrDefault();
                    deliveryBeingCreated.Orders.Add(order);
                }
                _siaqodb.StoreObject(deliveryBeingCreated);
            }
        
        }

        public void Handle(DeliveryRouted e)
        {
            var deliveryBeingRouted = Get(e.Id);
            deliveryBeingRouted.Routed = e.Routed;
            deliveryBeingRouted.Status = e.Status;
            deliveryBeingRouted.TotalDistance = e.TotalDistance;
            deliveryBeingRouted.TotalDuration = e.TotalDuration;
            lock (_siaqodb)
            {
                _siaqodb.StoreObject(deliveryBeingRouted);
            }
        }

        public void Handle(DeliveryStarted e)
        {
            var deliveryBeingStarted = Get(e.Id);
            deliveryBeingStarted.Started = e.Started;
            deliveryBeingStarted.Status = e.Status;

            var seed = e.Started;
            foreach (var order in deliveryBeingStarted.Orders)
            {
                seed = seed.Add(TimeSpan.FromSeconds(order.Duration));
                order.EstimatedArrivingTime = seed;
                order.Status = "Started";
            }

            lock (_siaqodb)
            {
                _siaqodb.StoreObject(deliveryBeingStarted);
            }
        }

        public void Handle(OrderCompleted e)
        {
            var delivery = Get(e.DeliveryId);
            if (delivery != null)
            {
                var isAllCOmpleted = delivery.Orders.All(x => x.Status == "Completed" || x.Id == e.Id);

                if (isAllCOmpleted)
                {
                    delivery.Completed = e.Completed;
                    delivery.Status = "Completed";

                    lock (_siaqodb)
                    {
                        _siaqodb.StoreObject(delivery);
                    }
                }
            }

        }
    }
}