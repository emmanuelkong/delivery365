﻿#region

using System;
using Delivery365.Events.Order;
using Delivery365.Events.Shared;
using Sqo.Attributes;

#endregion

namespace Delivery365.Models
{
    public class Order
    {
        [Index]
        [UniqueConstraint]
        public Guid Id { get; set; }

        public Address ShippingAddress { get; set; }
        public Customer Customer { get; set; }

        public string Status { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Planned { get; set; }
        public DateTime? Routed { get; set; }

        public DateTime? EstimatedArrivingTime;
        public long Duration;//Seconds
        public long Distance;//Meters

        public int Sequence;
        
        public DateTime? Completed { get; set; }
        public string ReceivedBy { get; set; }

        public Delivery Delivery { get; set; }
    }
}