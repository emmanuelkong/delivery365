﻿#region

using System;
using System.Collections.Generic;
using Sqo.Attributes;

#endregion

namespace Delivery365.Models
{
    public class Delivery
    {
        [Index]
        [UniqueConstraint]
        public Guid Id { get; set; }
        public string Status { get; set; }

        public List<Order> Orders { get; set; }

        public DateTime Created { get; set; }
        public DateTime Routed { get; set; }
        public DateTime Started { get; set; }
        public DateTime Completed { get; set; }

        public long TotalDuration { get; set; }
        public long TotalDistance { get; set; }

        public string OriginAddress;
        public string EndAddress;
    }
}