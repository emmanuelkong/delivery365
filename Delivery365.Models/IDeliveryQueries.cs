﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Delivery365.Models
{
    public interface IDeliveryQueries
    {
        Delivery Get(Guid id);
        List<Delivery> GetAll();
    }
}