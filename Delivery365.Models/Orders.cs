﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Delivery365.Events.Delivery;
using Delivery365.Events.Order;
using Edument.CQRS;
using Sqo;

#endregion

namespace Delivery365.Models
{
    public class Orders : IOrderQueries,
        ISubscribeTo<OrderCreated>,
        ISubscribeTo<OrderCompleted>,
        ISubscribeTo<OrderDeliveryPlanned>,
        ISubscribeTo<DeliveryRouted>
    {
        private readonly ISiaqodb _siaqodb;

        public Orders(ISiaqodb siaqodb)
        {
            _siaqodb = siaqodb;
        }

        public List<Order> GetAllOrders()
        {
            lock (_siaqodb)
                return (from Order order in _siaqodb
                    select order).OrderByDescending(x => x.Created).ToList();
        }

        public List<Order> GetPendingOrders()
        {
            lock (_siaqodb)
                return (from Order order in _siaqodb
                    where order.Status != "Completed"
                    select order).OrderByDescending(x => x.Created).ToList();
        }

        public Order Get(Guid id)
        {
            lock (_siaqodb)
                return (from Order order in _siaqodb
                    where order.Id == id
                    select order).SqoFirstOrDefault();
        }

        public void Handle(OrderCreated e)
        {
            Mapper.CreateMap<OrderCreated, Order>();

            var newOrder = Mapper.Map<Order>(e);

            lock (_siaqodb)
            {
                _siaqodb.StoreObject(newOrder);
            }
        }

        public void Handle(OrderDeliveryPlanned e)
        {
            lock (_siaqodb)
            {
                var associatingDelivery = (from Delivery delivery in _siaqodb
                    where delivery.Id == e.DeliveryId
                    select delivery).SqoFirstOrDefault();

                var planningOrder = Get(e.Id);

                if (associatingDelivery == null)
                {
                    throw new ObjectNotFoundExceptions("Delivery", e.DeliveryId);
                }

                if (planningOrder == null)
                {
                    throw new ObjectNotFoundExceptions("Order", e.Id);
                }

                planningOrder.Planned = e.DateTime;
                planningOrder.Delivery = associatingDelivery;
                planningOrder.Status = e.Status;

                _siaqodb.StoreObject(planningOrder);
            }
        }

        public void Handle(DeliveryRouted e)
        {
            foreach (var routeLeg in e.RouteLegs)
            {
                var order = Get(routeLeg.Id);

                order.Routed = e.Routed;
                order.Duration = routeLeg.Duration;
                order.Distance = routeLeg.Distance;
                order.Status = e.Status;
                lock (_siaqodb)
                {
                    _siaqodb.StoreObject(order);
                }
            }
            
        }

        public void Handle(OrderCompleted e)
        {
            lock (_siaqodb)
            {
                var completingOrder = Get(e.Id);

                if (completingOrder == null)
                {
                    throw new ObjectNotFoundExceptions("Order", e.Id);
                }

                completingOrder.Completed = e.Completed;
                completingOrder.Status = e.Status;

                _siaqodb.StoreObject(completingOrder);
            }
        }
    }
}