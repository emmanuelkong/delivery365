﻿#region

using System;

#endregion

namespace Delivery365.Models
{
    public class ObjectNotFoundExceptions : Exception
    {
        public ObjectNotFoundExceptions(string objectType, Guid id)
            : base(string.Format("{0} [{1}] not exist", objectType, id))
        {
        }
    }
}