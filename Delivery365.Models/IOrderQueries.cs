﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery365.Models
{
    public interface IOrderQueries
    {
        List<Order> GetAllOrders();
        List<Order> GetPendingOrders();
        Order Get(Guid id);
    }
}
