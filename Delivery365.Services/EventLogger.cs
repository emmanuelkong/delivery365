﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delivery365.Events.Order;
using Edument.CQRS;

namespace Delivery365.Services
{
    public class EventLogger : ISubscribeTo<OrderCreated>
    {
        public void Handle(OrderCreated e)
        {
            Debug.WriteLine("Order Id = {0} created at {1}",e.Id, e.Created);
        }
    }
}
