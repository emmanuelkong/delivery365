﻿#region

using System.Collections.Generic;
using System.Linq;
using Delivery365.Events.Delivery;
using Edument.CQRS;
using Newtonsoft.Json;
using RestSharp;

#endregion

namespace Delivery365.Services
{
    public class GoogleRouteService : ISubscribeTo<DeliveryRouted>
    {
        public void Handle(DeliveryRouted e)
        {
            var origin = e.OriginAddress;
            var destination = e.EndAddress;


            var client = new RestClient("http://maps.googleapis.com");
            var request = new RestRequest("maps/api/directions/json", Method.GET);

            request.AddParameter("origin", origin);
            request.AddParameter("destination", destination);

            var wayPointParameters = new List<string> {"optimize:true"};
            wayPointParameters.AddRange(e.RouteLegs.Select(x => x.Address));

            request.AddParameter("waypoints", string.Join("|", wayPointParameters));

            var response = client.Execute(request);
            var routeResult = JsonConvert.DeserializeObject<RouteResult>(response.Content);

            var route = routeResult.Routes.FirstOrDefault();

            if (route != null)
            {
                MapRouteResultToDeliveryRouted(route, e);
            }
        }

        private void MapRouteResultToDeliveryRouted(Route r, DeliveryRouted e)
        {
            e.TotalDistance = 0;
            e.TotalDuration = 0;

            //optimize way point sequence according result
            for (var i = 0; i < r.WaypointOrder.Count; i++)
            {
                e.RouteLegs[i].Sequence = r.WaypointOrder[i];
            }

            e.RouteLegs.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));


            for (var i = 0; i < e.RouteLegs.Count; i++)
            {
                var leg = r.Legs[i];
               
                e.TotalDuration += leg.Duration.Value;
                e.RouteLegs[i].Duration = leg.Duration.Value;

                e.TotalDistance += leg.Distance.Value;
                e.RouteLegs[i].Distance = leg.Distance.Value;
            }

            //calculate route leg to end address from last delivery address
            var endRouteLeg = r.Legs.Last();
            e.TotalDuration += endRouteLeg.Duration.Value;
            e.DurationToEndAddress = endRouteLeg.Duration.Value;
            e.TotalDistance += endRouteLeg.Distance.Value;
            e.DistanceToEndAddress = endRouteLeg.Distance.Value;

            e.MapUrl =
                string.Format(
                    "http://maps.googleapis.com/maps/api/staticmap?size=400x400&path=weight:3|color:blue|enc:{0}&sensor=false",
                    r.OverviewPolyline.Points.Replace(@"\\", @"\"));
        }
    }


    [JsonObject]
    public class RouteResult
    {
        [JsonProperty("routes")]
        public List<Route> Routes { get; set; }
    }

    public class Route
    {
        [JsonProperty("waypoint_order")]
        public List<int> WaypointOrder { get; set; }

        [JsonProperty("legs")]
        public List<Leg> Legs { get; set; }

        [JsonProperty("overview_polyline")]
        public OverviewPolyline OverviewPolyline { get; set; }
    }

    public class Leg
    {
        [JsonProperty("distance")]
        public TextValue Distance { get; set; }

        [JsonProperty("duration")]
        public TextValue Duration { get; set; }

        [JsonProperty("start_address")]
        public string StartAddress { get; set; }

        [JsonProperty("end_address")]
        public string EndAddress { get; set; }

        [JsonProperty("start_location")]
        public Location StartLocation { get; set; }

        [JsonProperty("end_location")]
        public Location EndLocation { get; set; }
    }

    public class OverviewPolyline
    {
        [JsonProperty("points")]
        public string Points { get; set; }
    }

    public class TextValue
    {
        public string Text { get; set; }
        public long Value { get; set; }
    }

    public struct Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}