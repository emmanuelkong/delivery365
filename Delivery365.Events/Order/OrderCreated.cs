﻿using System;
using System.Collections.Generic;
using Delivery365.Events.Shared;

namespace Delivery365.Events.Order
{
    public class OrderCreated
    {
        public Guid Id;
        public Address ShippingAddress;
        public DateTime Created;
        public Customer Customer;
        public string Status;
    }
}
