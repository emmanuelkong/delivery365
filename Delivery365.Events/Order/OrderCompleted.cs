﻿#region

using System;

#endregion

namespace Delivery365.Events.Order
{
    public class OrderCompleted
    {
        public DateTime Completed;
        public Guid Id;
        public Guid DeliveryId;
        public string Status;
        public string ReceivedBy;
        public bool DeliveryCompleted;
    }
}