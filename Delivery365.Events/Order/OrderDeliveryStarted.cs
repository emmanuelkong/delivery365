﻿#region

using System;

#endregion

namespace Delivery365.Events.Order
{
    public class OrderDeliveryStarted
    {
        public DateTime DateTime;
        public Guid Id;
    }
}