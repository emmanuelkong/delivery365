﻿#region

using System;

#endregion

namespace Delivery365.Events.Order
{
    public class OrderDeliveryPlanned
    {
        public DateTime DateTime;
        public Guid Id;
        public Guid DeliveryId;
        public string Status;
    }
}