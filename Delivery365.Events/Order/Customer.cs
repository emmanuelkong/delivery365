﻿using System;

namespace Delivery365.Events.Order
{
    public class Customer
    {
        public string Name;
        public string Phone;
        public string Email;

        public override string ToString()
        {
            return string.Format("{0}", Name);
        }
    }
}