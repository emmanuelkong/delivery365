﻿#region

using System;

#endregion

namespace Delivery365.Events.Order
{
    public class OrderDeliveryReceived
    {
        public DateTime DateTime;
        public Guid Id;
        public string Receiver;
    }
}