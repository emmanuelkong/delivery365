﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delivery365.Events.Delivery
{
    public class DeliveryStarted
    {
        public Guid Id;
        public DateTime Started;
        public string Status;
    }
}
