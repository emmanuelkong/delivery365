﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delivery365.Events.Delivery
{
    public class DeliveryItem
    {
        public Guid OrderId;
        public string Address;
    }
}
