﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Delivery365.Events.Delivery
{
    public class DeliveryRouteCalculated
    {
        public DateTime Routed;
        public Guid Id;
        public string Status;

        public string OriginAddress;
        public string EndAddress;

        public string MapUrl;

        public long TotalDuration;//Seconds
        public long TotalDistance;//Meters

        public List<RouteLeg> RouteLegs;

        public DateTime EstimatedArrivingTimeToEndAddress;
        public long DurationToEndAddress;//Seconds
        public long DistanceToEndAddress;//Meters
    }
}