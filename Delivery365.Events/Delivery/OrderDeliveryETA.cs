﻿#region

using System;

#endregion

namespace Delivery365.Events.Delivery
{
    public class RouteLeg
    {
        public DateTime EstimatedArrivingTime;

        public long Duration;//Seconds
        public long Distance;//Meters

        public Guid Id;
        public int Sequence;
        public string Address;
    }
}