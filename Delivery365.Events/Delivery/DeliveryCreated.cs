﻿using System;
using System.Collections.Generic;

namespace Delivery365.Events.Delivery
{
    public class DeliveryCreated
    {
        public Guid Id;
        public DateTime Created;
        public string Status;

        public string OriginAddress;
        public string EndAddress;
        public List<DeliveryItem> DeliveryItems;

    }
}
