﻿#region

using System.Text;

#endregion

namespace Delivery365.Events.Shared
{
    public class Address
    {
        public string Street1;
        public string Street2;
        public string Suburb;
        public int PostCode;
        public string State;

        public bool IsValid
        {
            get
            {
                var isInvalid = string.IsNullOrWhiteSpace(Street1) ||
                                string.IsNullOrWhiteSpace(Suburb) ||
                                string.IsNullOrWhiteSpace(State) ||
                                PostCode == 0;

                return !isInvalid;
            }
        }

        public override string ToString()
        {
            if (IsValid)
            {
                var addressBuilder = new StringBuilder();
                addressBuilder.AppendFormat("{0}, ", Street1);
                if (!string.IsNullOrWhiteSpace(Street2))
                {
                    addressBuilder.AppendFormat("{0}, ", Street2);
                }
                addressBuilder.AppendFormat("{0} {1} {2}", Suburb, State, PostCode);
                return addressBuilder.ToString();
            }
            return "Invalid Address";
        }
    }
}