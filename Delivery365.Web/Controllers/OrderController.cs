﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Delivery365.Domain.Order;
using Delivery365.Models;
using Delivery365.Web.Models;
using Humanizer;

#endregion

namespace Delivery365.Web.Controllers
{
    public class OrderController : ApiController
    {
        [HttpPut]
        public Guid PutOrder(CreateOrder cmd)
        {
            cmd.Id = Guid.NewGuid();
            cmd.DateTime = DateTime.UtcNow;
            Domain.Dispatcher.SendCommand(cmd);
            var createdOrder = Domain.OrderQueries.Get(cmd.Id);
            return createdOrder.Id;
        }

        [HttpGet]
        public Order GetOrder(Guid id)
        {
            var order = Domain.OrderQueries.Get(id);
            return order;
        }

        [HttpGet]
        public List<OrderViewModel> Pending()
        {
            var orders = Domain.OrderQueries.GetPendingOrders();
            return (from o in orders
                select new OrderViewModel
                {
                    Id = o.Id,
                    Customer = o.Customer.ToString(),
                    Created = o.Created.Humanize(),
                    EstimatedArrivingTime = o.EstimatedArrivingTime.Humanize(),
                    Completed = o.Completed.Humanize(),
                    ShippingAddress = o.ShippingAddress.ToString().Truncate(28),
                    Status = o.Status
                }).ToList();
        }
        
    }
}