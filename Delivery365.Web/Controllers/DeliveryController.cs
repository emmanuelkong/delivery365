﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Delivery365.Domain.Delivery;
using Delivery365.Domain.Order;
using Delivery365.Events.Delivery;
using Delivery365.Models;
using Delivery365.Web.Models;
using Humanizer;

#endregion

namespace Delivery365.Web.Controllers
{
    public class DeliveryController : ApiController
    {
        [HttpPut]
        public object Create(Guid[] orderIds)
        {
            var newId = Guid.NewGuid();

            var deliveryItems = new List<DeliveryItem>();

            foreach (var orderId in orderIds)
            {
                var order = Domain.OrderQueries.Get(orderId);
                if (order == null)
                {
                    throw new ObjectNotFoundExceptions("Order", orderId);
                }
                deliveryItems.Add(new DeliveryItem {OrderId = order.Id, Address = order.ShippingAddress.ToString()});
            }

            var companyAddress = ConfigurationManager.AppSettings["CompanyAddress"];
            var createDeliveryCmd = new CreateDelivery
            {
                Id = newId,
                DateTime = DateTime.UtcNow,
                DeliveryItems = deliveryItems,
                OriginAddress = companyAddress,
                EndAddress = companyAddress
            };
            Domain.Dispatcher.SendCommand(createDeliveryCmd);

            var routeDeliveryCmd = new RouteDelivery { DateTime = DateTime.UtcNow, Id = newId };
            Domain.Dispatcher.SendCommand(routeDeliveryCmd);

            return new {Id = newId};
        }

        [HttpGet]
        public DeliveryDetailViewModel GetDelivery(Guid id)
        {
            var delivery = Domain.DeliveryQueries.Get(id);
            return new DeliveryDetailViewModel
            {
                Id = delivery.Id,
                Created = delivery.Created.Humanize(),
                OrderCount = delivery.Orders != null ? delivery.Orders.Count : 0,
                Status = delivery.Status,
                TotalDistance = "{0} km".FormatWith(Math.Round((decimal)(delivery.TotalDistance / 1000), 1)),
                TotalDuration = TimeSpan.FromSeconds(delivery.TotalDuration).Humanize(),
                Description = GenerateDeliverySumary(delivery),
                Orders = (from Order o in delivery.Orders
                    select new OrderViewModel
                    {
                        Id = o.Id,
                        Created = o.Created.Humanize(),
                        EstimatedArrivingTime = o.EstimatedArrivingTime.ToString(),
                        Completed = o.Completed.ToString(),
                        Customer = o.Customer.ToString(),
                        ShippingAddress = o.ShippingAddress.ToString().Truncate(28),
                        Distance = "{0} km".FormatWith(Math.Round((decimal)(o.Distance / 1000),1)),
                        Duration = TimeSpan.FromSeconds(o.Duration).Humanize(),
                        Status = o.Status
                    }
                    ).ToList()
            };
        }

        [HttpGet]
        public List<DeliveryViewModel> GetAll()
        {
            var deliveries = Domain.DeliveryQueries.GetAll();
            return (from delivery in deliveries
                    select new DeliveryViewModel
                    {
                        Id = delivery.Id,
                        Created = delivery.Created.Humanize(),
                        OrderCount = delivery.Orders != null ? delivery.Orders.Count : 0,
                        Status = delivery.Status,
                        Description = GenerateDeliverySumary(delivery)
                        
                    }).ToList();
        }

        [HttpPost]
        public DeliveryDetailViewModel Start(StartDelivery cmd)
        {
            cmd.DateTime = DateTime.UtcNow;
            Domain.Dispatcher.SendCommand(cmd);
            return GetDelivery(cmd.Id);
        }

        [HttpPost]
        public DeliveryDetailViewModel Complete(CompleteOrder cmd)
        {
            cmd.DateTime = DateTime.UtcNow;
            Domain.Dispatcher.SendCommand(cmd);
            return GetDelivery(cmd.DeliveryId);
        }

        private string GenerateDeliverySumary(Delivery delivery)
        {
            return (from Order o in delivery.Orders select o.ShippingAddress.Suburb).Distinct().Humanize();
        }
    }
}