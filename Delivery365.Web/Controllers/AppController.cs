﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Delivery365.Web.Controllers
{
    public class AppController : Controller
    {
        public ActionResult Register()
        {
            return PartialView();
        }
        public ActionResult SignIn()
        {
            return PartialView();
        }
        public ActionResult Home()
        {
            return PartialView();
        }

        [Authorize]
        public ActionResult OrderList()
        {
            return PartialView();
        }

        [Authorize]
        public ActionResult OrderForm()
        {
            return PartialView();
        }

        [Authorize]
        public ActionResult DeliveryList()
        {
            return PartialView();
        }

        [Authorize]
        public ActionResult DeliveryDetail()
        {
            return PartialView();
        }
    }
}