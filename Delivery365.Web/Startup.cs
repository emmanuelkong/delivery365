﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Delivery365.Web.Startup))]

namespace Delivery365.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureIoc(app);
            ConfigureAuth(app);

            Domain.Setup();
        }
    }
}
