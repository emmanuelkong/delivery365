﻿angular.module('newDelivery', [])
    .controller('newDeliveryCtrl', ['$scope', '$http','$routeParams', function ($scope, $http, $routeParams) {

        $scope.master = {};
       
        $scope.save = function (delivery) {
            $scope.master = angular.copy(delivery);

            $http.put('/api/Delivery/Create/', delivery)
                .success(function (data, status, headers, config) {
                    window.location = '#/Delivery/data.id';
                });
        }

        $scope.reset = function () {
            $scope.order = angular.copy($scope.master);
        };

        $scope.cancel = function() {
            window.history.back();
        }

    $scope.reset();

    }]);