﻿angular.module('delivery', [])
    .controller('deliveryCtrl', [
        '$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

            $scope.getDelivery = function() {

                $http.get('/api/Delivery/GetDelivery/' + $routeParams.deliveryId)
                    .success(function(data, status, headers, config) {

                        var delivery = data || {};
                        $scope.delivery = delivery;
                    });
            }

            $scope.completeOrder = function(id) {
                var param = {
                    'id': id,
                    'deliveryId': $scope.delivery.id
                };
                $http.post('/api/Delivery/Complete/', param)
                    .success(function(data, status, headers, config) {
                        $scope.delivery = data;
                    });
            }

            $scope.startDelivery = function (id) {
                var param = {
                    'id': id
                };
                $http.post('/api/Delivery/Start/', param)
                 .success(function (data, status, headers, config) {
                     $scope.delivery = data;
                 });
            }

            $scope.getDelivery();

        }
    ]);