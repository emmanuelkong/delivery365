﻿angular.module('newOrder', [])
    .controller('newOrderCtrl', ['$scope', '$http', function ($scope, $http) {

        $scope.master = {};
       
        $scope.save = function (order) {
            $scope.master = angular.copy(order);

            $http.put('/api/Order/PutOrder/', order)
                .success(function (data, status, headers, config) {
                    window.location = '#/orders';
                });
        }

        $scope.reset = function () {
            $scope.order = angular.copy($scope.master);
        };

        $scope.cancel = function() {
            window.history.back();
        }

    $scope.reset();

    }]);