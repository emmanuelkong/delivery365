﻿angular.module('orders', [])
    .controller('ordersCtrl', ['$scope', '$http', 'filterFilter', function ($scope, $http, filterFilter) {

        $scope.getPendingOrders = function () {
            $http.get('/api/Order/Pending/')
                .success(function (data, status, headers, config) {
                    $scope.orders = data;
                });
        }

        $scope.createDelivery = function () {
            
            var selectedOrderIds = [];
            angular.forEach($scope.orders, function(value, key) {
                if (value.selected) {
                    selectedOrderIds.push(value.id);
                }
            });

            $http.put('/api/Delivery/Create/', selectedOrderIds)
              .success(function (data, status, headers, config) {
                  window.location = '#/delivery/' + data.id;
              });
        }

        $scope.getPendingOrders();

    }]);