﻿angular.module('deliveries', [])
    .controller('deliveriesCtrl', [
        '$scope', '$http', 'filterFilter', function($scope, $http, filterFilter) {

            $scope.getDeliveries = function() {
                $http.get('/api/Delivery/GetAll')
                    .success(function(data, status, headers, config) {
                        $scope.deliveries = data;
                    });
            }

            $scope.getDeliveries();

        }
    ]);