﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Humanizer;

namespace Delivery365.Web
{
    public static class Helpers
    {
        public static string Humanize(this DateTime? target, CultureInfo cultureInfo = null)
        {
            return target.HasValue ? target.Value.Humanize(true, null, cultureInfo ?? new CultureInfo("en-au")) : string.Empty;
        }

        public static string ToString(this DateTime? target, CultureInfo cultureInfo = null)
        {
            return target.HasValue ? target.Value.ToString(new CultureInfo("en-au").DateTimeFormat) : string.Empty;
        }
    }
}