﻿#region

using System.Diagnostics;

#endregion

namespace Delivery365.Web
{
    public interface ILogger
    {
        void Write(string message, params object[] args);
    }

    public class Logger : ILogger
    {
        public void Write(string message, params object[] args)
        {
            Debug.WriteLine(message, args);
        }
    }
}