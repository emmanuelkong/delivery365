﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using Autofac.Integration.WebApi;
using Delivery365.Events.Shared;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Logging;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Practices.ServiceLocation;
using Owin;
using Delivery365.Web.Providers;
using Delivery365.Web.Models;
using Sqo;

namespace Delivery365.Web
{
    public partial class Startup
    {
        public void ConfigureIoc(IAppBuilder app)
        {
            SiaqodbConfigurator.SetLicense("0CK1mgRTUAc7UfS3F8+HFPEkrBF0gglKuOxAt2Ddg+Y=");

            var builder = new ContainerBuilder();
            builder.Register(c => new Logger()).As<ILogger>().InstancePerLifetimeScope();
            builder.Register(c => new Siaqodb(HttpContext.Current.Server.MapPath("~/App_Data"))).As<ISiaqodb>().SingleInstance();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();
            app.UseAutofacMiddleware(container);

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            app.UseAutofacWebApi(GlobalConfiguration.Configuration);

            // Set the service locator to an AutofacServiceLocator
            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));
        }
    }
}
