﻿#region

using System.Threading.Tasks;
using Delivery365.Web.Models;
using Microsoft.AspNet.Identity;
using Sqo;

#endregion

namespace Delivery365.Web
{
    public class SiaqodbUserStore : IUserStore<User>, IUserPasswordStore<User>
    {
        private readonly ISiaqodb _siaqodb;

        public SiaqodbUserStore(ISiaqodb siaqodb)
        {
            _siaqodb = siaqodb;
        }

        public void Dispose()
        {
        }

        public Task CreateAsync(User user)
        {
            return _siaqodb.StoreObjectAsync(user);
        }

        public Task UpdateAsync(User user)
        {
            return _siaqodb.StoreObjectAsync(user);
        }

        public Task DeleteAsync(User user)
        {
            return _siaqodb.DeleteAsync(user);
        }

        public Task<User> FindByIdAsync(string userId)
        {
            return (from User u in _siaqodb where u.Id == userId select u).SqoFirstOrDefaultAsync();
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return (from User u in _siaqodb where u.UserName == userName select u).SqoFirstOrDefaultAsync();
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return _siaqodb.StoreObjectAsync(user);
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return new Task<string>(() => user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return new Task<bool>(() => !string.IsNullOrEmpty(user.PasswordHash));
        }
    }
}