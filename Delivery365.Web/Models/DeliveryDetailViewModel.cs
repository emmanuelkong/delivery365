﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delivery365.Web.Models
{
    public class DeliveryDetailViewModel
    {
        public Guid Id;
        public string Created;
        public int OrderCount;
        public string Status { get; set; }
        public string Description { get; set; }

        public string TotalDuration { get; set; }
        public string TotalDistance { get; set; }

        public List<OrderViewModel> Orders { get; set; }

        public bool ReadyToStart
        {
            get { return Status == "Routed"; }
        }
    }
}