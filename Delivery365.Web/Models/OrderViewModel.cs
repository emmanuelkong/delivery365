﻿#region

using System;

#endregion

namespace Delivery365.Web.Models
{
    public class OrderViewModel
    {
        public string Completed;
        public string Created;
        public string EstimatedArrivingTime;
        public Guid Id;
        public string ShippingAddress { get; set; }
        public string Customer { get; set; }
        public string Status { get; set; }

        public bool TobeCompleted
        {
            get { return Status == "Started"; }
        }

        public string Duration { get; set; }
        public string Distance { get; set; }
    }
}