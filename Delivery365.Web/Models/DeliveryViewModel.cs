﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delivery365.Web.Models
{
    public class DeliveryViewModel
    {
        public Guid Id;
        public string Created;
        public int OrderCount;
        public string Status { get; set; }
        public string Description { get; set; }
    }
}