﻿#region

using Delivery365.Domain.Delivery;
using Delivery365.Domain.Order;
using Delivery365.Models;
using Delivery365.Services;
using Edument.CQRS;
using Microsoft.Practices.ServiceLocation;
using Sqo;

#endregion

namespace Delivery365.Web
{
    public class Domain
    {
        public static MessageDispatcher Dispatcher;
        public static IOrderQueries OrderQueries;
        public static IDeliveryQueries DeliveryQueries;

        public static EventLogger EventLogger;
        public static GoogleRouteService GoogleRouteService;

        public static void Setup()
        {
            var siaqodb = ServiceLocator.Current.GetInstance<ISiaqodb>();

            //Dispatcher = new MessageDispatcher(new SiaqodbEventStore(siaqodb));
            Dispatcher = new MessageDispatcher(new InMemoryEventStore());

            Dispatcher.ScanInstance(new OrderAggregate());
            Dispatcher.ScanInstance(new DeliveryAggregate());

            GoogleRouteService = new GoogleRouteService();
            Dispatcher.ScanInstance(GoogleRouteService);

            OrderQueries = new Orders(siaqodb);
            DeliveryQueries = new Deliveries(siaqodb);
            Dispatcher.ScanInstance(OrderQueries);
            Dispatcher.ScanInstance(DeliveryQueries);

            EventLogger = new EventLogger();
            Dispatcher.ScanInstance(EventLogger);

            
        }
    }
}