﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Edument.CQRS;
using Sqo;

#endregion

namespace Delivery365.Web
{
    public class SiaqodbEventStore : IEventStore
    {
        private readonly ISiaqodb _siaqodb;

        public SiaqodbEventStore(ISiaqodb siaqodb)
        {
            _siaqodb = siaqodb;
        }

        public IEnumerable LoadEventsFor<TAggregate>(Guid id)
        {
            var aggregate =
                (from AggregateEntity a in _siaqodb where a.Id == id select a).SqoFirstOrDefault
                    ();

            if (aggregate != null && aggregate.Events != null)
            {
                foreach (var eventEntity in aggregate.Events)
                {
                    yield return DeserializeEvent(eventEntity.Type, eventEntity.Body);
                }
            }
        }

        public void SaveEventsFor<TAggregate>(Guid id, int eventsLoaded, ArrayList newEvents)
        {
            var aggregate =
                (from AggregateEntity a in _siaqodb where a.Id == id select a).SqoFirstOrDefault
                    () ??
                new AggregateEntity
                {
                    Id = id,
                    Events = new List<EventEntity>(),
                    Type = typeof (TAggregate).AssemblyQualifiedName
                };

            // Ensure no events persisted since us.
            if (aggregate.Events.Count != eventsLoaded)
                throw new Exception("Concurrency conflict; cannot persist these events");

            foreach (var e in newEvents)
            {
                var eventEntity = new EventEntity
                {
                    Type = e.GetType().AssemblyQualifiedName,
                    Body = SerializeEvent(e)
                };

                aggregate.Events.Add(eventEntity);
            }

            _siaqodb.StoreObject(aggregate);
        }

        private object DeserializeEvent(string typeName, string data)
        {
            var ser = new XmlSerializer(Type.GetType(typeName));
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
            ms.Seek(0, SeekOrigin.Begin);
            return ser.Deserialize(ms);
        }

        private string SerializeEvent(object obj)
        {
            var ser = new XmlSerializer(obj.GetType());
            var ms = new MemoryStream();
            ser.Serialize(ms, obj);
            ms.Seek(0, SeekOrigin.Begin);
            return new StreamReader(ms).ReadToEnd();
        }
    }

    public class AggregateEntity
    {
        public List<EventEntity> Events;
        public Guid Id;
        public string Type;
    }

    public class EventEntity
    {
        public string Body;
        public string Type;
    }
}