﻿using System;

namespace Delivery365.Domain.Delivery
{
    public class StartDelivery
    {
        public Guid Id { get; set; }
        public DateTime DateTime { get; set; }
    }
}
