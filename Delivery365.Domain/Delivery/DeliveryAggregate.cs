﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.Internal;
using Delivery365.Events.Delivery;
using Delivery365.Events.Order;
using Edument.CQRS;

#endregion

namespace Delivery365.Domain.Delivery
{
    public class DeliveryAggregate : Aggregate,
        IHandleCommand<CreateDelivery>,
        IHandleCommand<RouteDelivery>,
        IHandleCommand<StartDelivery>,
        IApplyEvent<DeliveryCreated>,
        IApplyEvent<DeliveryRouted>,
        IApplyEvent<OrderDeliveryPlanned>,
        IApplyEvent<StartDelivery>
    {
        private readonly List<DeliveryItem> _deliveredItems = new List<DeliveryItem>();
        private readonly List<DeliveryItem> _outstandingDeliveryItems = new List<DeliveryItem>();

        private bool _completed;
        private bool _routed;
        private bool _started;
        public string _originAddress;
        public string _endAddress;

        public void Apply(DeliveryCreated e)
        {
            _outstandingDeliveryItems.AddRange(e.DeliveryItems);
            _originAddress = e.OriginAddress;
            _endAddress = e.EndAddress;

        }

        public void Apply(DeliveryRouted e)
        {
            _routed = true;
        }

        public void Apply(StartDelivery e)
        {
            _started = true;
        }

        public IEnumerable Handle(CreateDelivery c)
        {
            if (c.DeliveryItems == null || c.DeliveryItems.Count == 0)
            {
                throw new DeliveryMustNotBeEmpty();
            }

            Mapper.CreateMap<CreateDelivery, DeliveryCreated>();

            var deliveryPlanCreated = Mapper.Map<DeliveryCreated>(c);
            deliveryPlanCreated.Created = c.DateTime;
            deliveryPlanCreated.Status = "New";
            yield return deliveryPlanCreated;

            foreach (var deliveryItem in c.DeliveryItems)
            {
                yield return
                    new OrderDeliveryPlanned
                    {
                        DateTime = c.DateTime,
                        Id = deliveryItem.OrderId,
                        DeliveryId = deliveryPlanCreated.Id,
                        Status = "Planned"
                    };
            }
        }

        public IEnumerable Handle(StartDelivery c)
        {
            if (_started)
            {
                throw new DeliveryAlreadyStarted();
            }
           
            Mapper.CreateMap<StartDelivery, DeliveryStarted>();
            var deliveryStarted = Mapper.Map<DeliveryStarted>(c);
            deliveryStarted.Started = c.DateTime;
            deliveryStarted.Status = "Started";

            yield return deliveryStarted;
        }

        public IEnumerable Handle(RouteDelivery c)
        {
            if (_routed)
            {
                throw new DeliveryAlreadyRouted();
            }

            if (!IsAnyOutStandingOrders())
            {
                throw new DeliveryNotOutstanding();
            }

            Mapper.CreateMap<RouteDelivery, DeliveryRouted>();
            var deliveryRouted = Mapper.Map<DeliveryRouted>(c);
            deliveryRouted.Routed = c.DateTime;
            deliveryRouted.OriginAddress = _originAddress;
            deliveryRouted.EndAddress = _endAddress;
            deliveryRouted.RouteLegs =
                _outstandingDeliveryItems.Select(
                    x => new RouteLeg {Id = x.OrderId, Address = x.Address}).ToList();
            deliveryRouted.Status = "Routed";
        
            yield return deliveryRouted;
        }

       
        public bool IsAnyOutStandingOrders()
        {
            return _outstandingDeliveryItems.Any();
        }

        public void Apply(OrderDeliveryPlanned e)
        {
            
        }
    }
}