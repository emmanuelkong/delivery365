﻿using System;

namespace Delivery365.Domain.Delivery
{
    public class DeliveryNotOutstanding : Exception
    {
    }

    public class DeliveryAlreadyStarted : Exception
    {
    }

    public class DeliveryAlreadyEstimated : Exception
    {
    }

    public class DeliveryNotStarted : Exception
    {
    }

    public class DeliveryNotRouted : Exception
    {

    }

    public class DeliveryAlreadyRouted : Exception
    {
        
    }

    public class DeliveryMustNotBeEmpty : Exception
    {

    }
}
