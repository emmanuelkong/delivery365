﻿using System;
using System.Collections.Generic;
using Delivery365.Events.Delivery;

namespace Delivery365.Domain.Delivery
{
    public class CreateDelivery
    {
        public Guid Id;
        public DateTime DateTime;
        public string OriginAddress;
        public string EndAddress;
        public List<DeliveryItem> DeliveryItems;
    }
}
