﻿#region

using System;

#endregion

namespace Delivery365.Domain.Order
{
    public class CompleteOrder
    {
        public DateTime DateTime;
        public Guid Id;
        public Guid DeliveryId;
        public string ReceivedBy;
    }
}