﻿#region

using System;
using System.Collections;
using AutoMapper;
using Delivery365.Events.Delivery;
using Delivery365.Events.Order;
using Delivery365.Events.Shared;
using Edument.CQRS;

#endregion

namespace Delivery365.Domain.Order
{
    public class OrderAggregate : Aggregate,
        IHandleCommand<CreateOrder>,
        IHandleCommand<CompleteOrder>,
        IApplyEvent<OrderCreated>,
        IApplyEvent<OrderCompleted>,
        ISubscribeTo<OrderDeliveryPlanned>
    {

        public IEnumerable Handle(CompleteOrder c)
        {
            Mapper.CreateMap<CompleteOrder, OrderCompleted>();

            var orderCompleted = Mapper.Map<OrderCompleted>(c);
            orderCompleted.Completed = c.DateTime;
            orderCompleted.Status = "Completed";

            yield return orderCompleted;
        }

        public IEnumerable Handle(CreateOrder c)
        {
            Mapper.CreateMap<CreateOrder, OrderCreated>();
            Mapper.CreateMap<Customer, Customer>();
            Mapper.CreateMap<Address, Address>();

            var orderCreated = Mapper.Map<OrderCreated>(c);
            orderCreated.Created = c.DateTime;
            orderCreated.Status = "New";
            yield return orderCreated;
        }

        public void Handle(OrderDeliveryPlanned e)
        {
            //throw new NotImplementedException();
        }

        public void Apply(OrderCreated e)
        {
        }

        public void Apply(OrderCompleted e)
        {
        }
    }
}