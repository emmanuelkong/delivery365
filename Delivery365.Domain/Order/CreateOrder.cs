﻿#region

using System;
using Delivery365.Events.Order;
using Delivery365.Events.Shared;

#endregion

namespace Delivery365.Domain.Order
{
    public class CreateOrder
    {
        public DateTime DateTime;
        public Guid Id;
        public Address ShippingAddress { get; set; }
        public Customer Customer { get; set; }
    }
}